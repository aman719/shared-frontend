import React from 'react';

function DeclarationToolTip() {
  return (
    <div>
        <div class="tooltip">Declaration
            <span class="tooltiptext">
            •	For the purpose of this Declaration, the words ‘I’, ‘my’ and ‘me’ refer to the Property Owner.
            •	I hereby declare that all the details entered / furnished / uploaded above are true and correct to the best of my knowledge and  belief  and  I  undertake  to  inform  NebourHood of  any  changes  therein,  immediately.  
            •	Further, I am giving an approval of it’s display by NebourHood, implying that I either own the rights to the information entered / furnished / uploaded or that I have a license or permission to use it from the copyright holder, thereby agreeing to abide by all the policies of NebourHood. 
            •	In case any of the above information is found to be false / untrue / misleading / misrepresenting, I am aware that it would be at my own discretion and responsibility and that I would be held liable for it.  
            •	NebourHood would not be held responsible under any circumstances whatsoever, for any information entered / furnished / uploaded by me.
            </span>
        </div>
    </div>
  )
}

export default DeclarationToolTip;