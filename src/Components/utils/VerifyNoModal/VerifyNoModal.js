import React from "react";
import "./VerifyNoModal.css";

export default function VerifyNoModal(props) {
  return (
    <div className="verify-no-modal-container">
        <span onClick={props.onClose} className="material-icons">
            close
        </span>
      <div className="verify-no-modal">
        <p className="title">Verify Phone Number</p>
        <input onChange={props.onChange} type="number" placeholder="Enter OTP" />
        <button onClick={props.onClick} className="otp-verify-btn">Verify </button>
      </div>
    </div>
  );
}
