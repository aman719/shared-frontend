import React from "react";
import "./SP_ViewImg.css";

export default function SPViewImg(props) {


  return (
    <div style={{
      transform:props.transform
    }} className="view-img-container">
      <span onClick={props.handlecloseview} className="material-icons">
        cancel
      </span>
      <br/>
      <img src={props.content} />
    </div>
  );
}
