module.exports.languages = [
  "Assamese",
  "English",
  "Gujarati",
  "Kannada",
  "Malayalam",
  "Marathi",
  "Odia",
  "Punjabi",
  "Tamil",
  "Telugu",
  "Urdu",
];

module.exports.personalhobbies = [
  "Cricket",
  "Volleyball",
  "Badminton",
  "Chess",
  "Football",
  "Ludo",
];

module.exports.personalawards = [
  "Bharat Ratna",
  "Padma Vibhushan",
  "Padma Shri",
];
module.exports.professionalawards = [
  "Bharat Ratna",
  "Padma Vibhushan",
  "Padma Shri",
];

module.exports.professionalskills = ["Personal Skills 1", "Personal skills 2"];
module.exports.workdays = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];

module.exports.initialconsultations = ["Company Office", "Site"];

module.exports.softwaresused = [
  "Architectural Construction",
  "Civil Engineering",
];
module.exports.designschematics = [
  "Plans",
  "Drawings / Sketches",
  "Designs",
  "Consultation",
  "Renders",
  "Walk Through",
];

module.exports.typeofpropertiesserved = {
  Residential: [
    "Apartment",
    "Penthouse",
    "Stand Alone building",
    "Independent Home",
    "Villa",
    "Bungalow",
  ],
  Commercial: [
    "Retail",
    "Hospitality - (Accommodation, Food & Beverage, Travel & Tourism)",
    "Healthcare",
    "Independent Home",
    "Office - Corporate & Tech",
  ],
  Institutional: [
    "Educational Sector",
    "Financial Sector",
    "Media & Entertainment Sector",
    "Research & Development Sector",
    "Transportation Sector",
  ],
};

module.exports.InteriorDesignExecutionServices = [
  "Hard Finishes",
  "Furniture, Fixtures & Equipment",
  "Soft Finishes",
  "Art & Décor",
  "Eco – Friendly",
  "Entertainment",
  "Tech – Savvy",
  "Health & Safety",
  " Security Systems",
  "Disabled / Handicap – Accessible Designs & Services",
];

module.exports.ArchitecturalConstructionExecutionServices = {
    GreyStructure : [
       "Soil Testing", 
       "Site Clearance", 
       "Site Layout", 
       "Substructure Construction Phase", 
       "Super Structure Construction Phase", 
       "Disabled / Handicap – Accessible Designs & Services", 
       "Health & Safety", 
       "Security Systems", 
       "Finishing", 
    ]
}

module.exports.CivilRenovationServices = [
    "Same as the above selected Interior Design Execution Services",
    "Same as the above selected Architectural Construction Execution Services Selected"
]
module.exports.OtherServices = [
    "Assembling Furniture",
    "Heavy Item Lifting and Moving",
    "Household Chores",
    "Packers and Movers",
    "Pest Control",
  
]

module.exports.ProductsAccessories = [
    "Appliances",
    "Artwork",
    "Décor",
    "Furniture",
    "Hardware",
    "Mirrors",
    "Modular / Customized Furniture",
    "Painting",
    "Upholstery",
    "Wallpapers",
  
]

module.exports.FinalReviewHandover = [
    "Final Building Inspection",
    "Confirmation of Final Payment Received",
    "Documentation",
    "Handover",
    "Photoshoot & Videography",
]

module.exports.DesignSpecialization = {
    InteriorDesignStyles : [
        "Beach / Nautical",
        "Contemporary Home Décor",
        "Craftsman",
        "Eclectic",
        "Eco - Friendly",
        "Farmhouse",
        "Hollywood Glam",
        "Industrial",
        "Mediterranean",
        "Mid – Century Modern",
        "Minimalist",
        "Modern",
        "Rustic",
        "Scandinavian",
        "Shabby – Chic",
        "Southwestern",
        "Traditional",
        "Tropical",
    ],
    ArchitecturalStyles
 : [
        "Art Deco",
        "Bauhaus",
        "Constructivism (Art)",
        "Constructivist Architecture",
        "Contemporary Architecture",
        "Eco - Friendly",
        "Functionalism",
        " Futurist Architecture",
        "High - Tech Architecture",
        "Mid – Century Modern",
        "Industrial Architecture",
        "International Style",
        "Mid - Century Modern",
        "Minimalism",
        "Modern Architecture",
        "New Objectivity",
        "Organic Architecture",
        "Postmodern Architecture",
        "Ranch - Style House",
        "Streamline",
        "Sustainable Architecture",
    ]
}