import React, { useState } from "react";
import EditProfile from "./EditProfile/EditProfile";
import "./EditProfileandOverview.css";
import ProfileOverview from "./ProfileOverview/ProfileOverview";

export default function EditProfileandOverview() {
  const [editclicked, seteditclicked] = useState(false);

  const handleedit = (e) => {
    seteditclicked(!editclicked);
  };

  return (
    <div className="edit-profile-and-overview-container">
      <div className="edit-profile-and-overview">
        <button
          style={{
            visibility: editclicked ? "hidden" : "visible",
          }}
          onClick={handleedit}
          className="edit-and-goback-btn"
        >
          Edit Profile
        </button>

        {editclicked ? <EditProfile /> : <ProfileOverview />}
      </div>
    </div>
  );
}
