import React from "react";
import "./ProjectOverview.css";

function ProjectOverview(props) {
  return (
    <div className="project-overview-container">
      <h3 className="title">Project Overview</h3>
      <div className="project-overview">
        <div className="project-detail">
          <label>Project Name</label>
          <p>{props.ProjectDetails.Project_Name}</p>
        </div>
        <div className="project-detail">
          <label>Project Client Name</label>
          <p>{props.ProjectDetails.Project_Client_Name}</p>
        </div>
        <div className="project-detail">
          <label>Project Year of Completion</label>
          <p>{props.ProjectDetails.Project_Completion}</p>
        </div>
        <div className="project-detail">
          <label>Project Value in Indian Rupee</label>
          <p>{props.ProjectDetails.Project_Value}</p>
        </div>

        <div className="project-detail">
          <label>
            Project Page link to Company Website / Company YouTube Channel URL
          </label>
          <p>{props.ProjectDetails.Project_Page_Link}</p>
        </div>
        <div className="project-detail">
          <label>Project Description</label>
          <p>{props.ProjectDetails.Project_Description}</p>
        </div>
        <div className="project-detail">
          <label>Project Service Requirement</label>
          <p>{props.ProjectDetails.Service_Requirement}</p>
        </div>
        <div className="project-detail"></div>
      </div>
    </div>
  );
}

export default ProjectOverview;
