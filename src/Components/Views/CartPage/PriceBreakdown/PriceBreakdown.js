import React from "react";
import Razorpay from "../../../utils/Razorpay/SP_subscription/Razorpay";
import "./PriceBreakdown.css";
import { BABY_BEAR_PRICE, MAMA_BEAR_PRICE, PAPA_BEAR_PRICE, TEDDY_BEAR_PRICE } from "../../../../PricingValidityConfig";

const PriceBreakdown = (props) => {
  const plan = "Baby Bear";

  return (
    <div style={props.styles} className="plan-breakdown-container">
      <div className="plan-breakdown">
        <span onClick={props.handleclose} id="close" className="material-icons">
          cancel
        </span>
        <div className="plan-breakdown-details">
          <div className="plan-title-and-price">
            <p className="title">{plan.Name}</p>
            <p className="pricing">{plan.Price}</p>
          </div>
          <p className="features-title">Features :</p>
          <div className="features-container">
           
            <div className="billing-details">
              <div className="fee-and-taxes">
                <h3 style={{ textAlign: 'center' }}>Amount Per Month</h3>
                <div className="fee">
                  <p className="title">Regular Leads:</p>
                  <p className="amount">{plan.Name === "Baby Bear" ? 28 : plan.Name === "Baby Tiger" ? 13 : plan.Name === "Startup Bear" ? 4 : plan.Name === "Startup Tiger" ? 2 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">Extra FREE Leads:</p>
                  <p className="amount">{plan.Name === "Baby Bear" ? 7 : plan.Name === "Baby Tiger" ? 4 : plan.Name === "Startup Bear" ? 2 : plan.Name === "Startup Tiger" ? 1 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">Total No. of Leads:</p>
                  <p className="amount">{plan.Name === "Baby Bear" ? 35 : plan.Name === "Baby Tiger" ? 17 : plan.Name === "Startup Bear" ? 7 : plan.Name === "Startup Tiger" ? 3 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">Cost Per Regular Lead:</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? 175 : plan.Name === "Baby Tiger" ? 375 : plan.Name === "Startup Bear" ? 175 : plan.Name === "Startup Tiger" ? 375 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">Platform Fee:</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? 100 : plan.Name === "Baby Tiger" ? 124 : plan.Name === "Startup Bear" ? 99 : plan.Name === "Startup Tiger" ? 49 : 0}</p>
                </div>
                {/* <div className="fee">
                  <p className="title">Amount (Exclusive of G.S.T.):</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? 4999 : plan.Name === "Papa Bear" ? 3999 : plan.Name === "Mama Bear" ? 2999 : plan.Name === "Teddy Bear" ? 1999 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">G.S.T. Amount (18%):</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? 0 : plan.Name === "Papa Bear" ? 0 : plan.Name === "Mama Bear" ? 0 : plan.Name === "Teddy Bear" ? 0 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">Final Amount (Inclusive of G.S.T.):</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? 4999 : plan.Name === "Papa Bear" ? 3999 : plan.Name === "Mama Bear" ? 2999 : plan.Name === "Teddy Bear" ? 1999 : 0}</p>
                </div> */}
                <br />
                <h3 style={{ textAlign: 'center' }}>Duration</h3>
                <div className="fee">
                  <p className="title">{plan.Name === "Startup Bear" ? "No. of Days:" : plan.Name === "Startup Tiger" ? "No. of Days:" : "No. of Months:"}</p>
                  <p className="amount">{plan.Name === "Baby Bear" ? 1 : plan.Name === "Papa Bear" ? 3 : plan.Name === "Mama Bear" ? 6 : plan.Name === "Teddy Bear" ? 12 : plan.Name === "Baby Tiger" ? 1 : plan.Name === "Startup Bear" ? 7 : plan.Name === "Startup Tiger" ? 7 : 0}</p>
                </div>
                <br />
                <h3 style={{ textAlign: 'center' }}>Amount To Be Paid</h3>
                <div className="fee">
                  <p className="title">Amount (Exclusive of G.S.T.) * {plan.Name === "Baby Bear" ? 1 : plan.Name === "Papa Bear" ? 3 : plan.Name === "Mama Bear" ? 6 : plan.Name === "Teddy Bear" ? 12 : plan.Name === "Baby Tiger" ? 1 : plan.Name === "Startup Bear" ? 1 : plan.Name === "Startup Tiger" ? 1 : 0}</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? BABY_BEAR_PRICE : plan.Name === "Papa Bear" ? PAPA_BEAR_PRICE : plan.Name === "Mama Bear" ? MAMA_BEAR_PRICE : plan.Name === "Teddy Bear" ? TEDDY_BEAR_PRICE : plan.Name === "Baby Tiger" ? 4999 : plan.Name === "Startup Bear" ? 799 : plan.Name === "Startup Tiger" ? 799 : 0}</p>
                </div>
                {/* <div className="fee">
                  <p className="title">G.S.T. Amount (18%) * {plan.Name === "Baby Bear" ? 1 : plan.Name === "Papa Bear" ? 3 : plan.Name === "Mama Bear" ? 6 : plan.Name === "Teddy Bear" ? 12 : 0}:</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? 0 : plan.Name === "Papa Bear" ? 0 : plan.Name === "Mama Bear" ? 0 : plan.Name === "Teddy Bear" ? 0 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">Final Amount (Inclusive of G.S.T.) * {plan.Name === "Baby Bear" ? 1 : plan.Name === "Papa Bear" ? 3 : plan.Name === "Mama Bear" ? 6 : plan.Name === "Teddy Bear" ? 12 : 0}:</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? BABY_BEAR_PRICE : plan.Name === "Papa Bear" ? PAPA_BEAR_PRICE : plan.Name === "Mama Bear" ? MAMA_BEAR_PRICE : plan.Name === "Teddy Bear" ? TEDDY_BEAR_PRICE : 0}</p>
                </div> */}
                <br />
                {/* {plan.Name === "Baby Bear" ? null : 
                <>
                  <h3 style={{ textAlign: 'center' }}>Total Amount Saved</h3>
                  <div className="fee">
                    <p className="title">Amount Saved:</p>
                    <p className="amount">{plan.Name === "Baby Bear" ? null : plan.Name === "Papa Bear" ? 20.0040008 : plan.Name === "Mama Bear" ? 40.0080016 : plan.Name === "Teddy Bear" ? 60.0120024 : 0} %</p>
                  </div>
                </>
                } */}
              </div>
              <br />
              <button className="proceed">
                {/* <Razorpay
                  Name={plan.Name}
                  Price={plan.Price}
                  Validity={plan.Validity}
                /> */}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PriceBreakdown;
