const logos = [
    {
        link : "https://templates.envytheme.com/fido/default/assets/images/partner/partner-1.png",
        name : "logotext"
    },
    {
        link : "https://templates.envytheme.com/fido/default/assets/images/partner/partner-2.png",
        name : "palace"
    },
    {
        link : "https://templates.envytheme.com/fido/default/assets/images/partner/partner-3.png",
        name : "housetalk"
    },
    {
        link : "https://templates.envytheme.com/fido/default/assets/images/partner/partner-4.png",
        name : "infinityhouse"
    },
    {
        link : "https://templates.envytheme.com/fido/default/assets/images/partner/partner-5.png",
        name : "estaterise"
    },
    {
        link : "https://templates.envytheme.com/fido/default/assets/images/partner/partner-6.png",
        name : "trustestate"
    },
    {
        link : "https://templates.envytheme.com/fido/default/assets/images/partner/partner-7.png",
        name : "realestate1"
    },
    {
        link : "https://templates.envytheme.com/fido/default/assets/images/partner/partner-8.png",
        name : "realestate2"
    },
    {
        link : "https://templates.envytheme.com/fido/default/assets/images/partner/partner-9.png",
        name : "realestate3"
    },
    {
        link : "https://templates.envytheme.com/fido/default/assets/images/partner/partner-10.png",
        name : "realestate4"
    },

]

module.exports.logos = logos;