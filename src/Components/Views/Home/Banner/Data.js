const Data = [
    {
        Title : "Location",
        Collection : ["India" , "Germany" , "Australia" , "New Zealand"  , "Sweden" , "America"]
    },
    {
        Title : "Property",
        Collection : ["Cafe" , "Bar" , "Restaurant", "Apartment" , "House" , "Farm" , "Office"]
    }
]

module.exports.Data = Data;