import React, { useLayoutEffect, useState } from "react";
import "./NewProperties.css";
import Button from "../../../utils/Button/Button"
import { NewAddedProperties } from "./Data";

export default function NewProperties() {
  const bodywidth = document.querySelector("body").scrollWidth;

  const [screenwidth, setscreenwidth] = useState(bodywidth);

  useLayoutEffect(() => {
    window.addEventListener("resize", () => {
      setscreenwidth(window.innerWidth);
      
    });
  }, [screenwidth]);

  


  return (
    <div  className="newproperties-container">
      <h1 className="title">New Added Properties</h1>
      <br />
      <p style={{width:screenwidth<=800 ? screenwidth-100 : null }} className="description">
        Proin gravida nibh vel velit auctor aliquet aenean sollicitudin lorem
        quis bibendum auctor nisi elit consequat ipsum nec sagittis sem nibh id
        elit.
      </p>
      <div className="new-properties">
        {NewAddedProperties.map((property, index) => {
          return (
            <div style={{width:screenwidth<=800 ? screenwidth-20 : null }}   id="new-added-property"  key={index} className="new-added-property">
              <div className="imageandtype">
                <p className="for">{property.For}</p>
                <img style={{width:screenwidth<=800 ? screenwidth-30 : null }} src={property.Image} alt={property.Name} />
                <span style={{width:screenwidth<=800 ? screenwidth-30 : null }} className="price" ><h3 style={{color:"white"}}>$&nbsp;{property.Price}/ </h3>Per Month</span>
              </div>
              <div style={{width:screenwidth<=800 ? screenwidth-20 : null }} className="details">
                <p className="address">{property.Address}</p>
                <h2 className="name">{property.Name}</h2>
                <div className="otherdetails">
                  <i className="fas fa-bed"></i>&nbsp;{property.Bedrooms}{" "}
                  Bedrooms&nbsp;&nbsp;&nbsp;
                  <i className="fas fa-bath"></i>&nbsp;{property.Baths}{" "}
                  Baths&nbsp;&nbsp;&nbsp;
                 
                  <i className="fas fa-car"></i>&nbsp;&nbsp;
                  {property.FreeParking ? "Free Parking" : null}
                </div>
                <div className="rating">
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                <i className="fas fa-star"></i>
                &nbsp;
                <p>Average</p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <br/>
      <div className="btn">
      <Button width="220px" background="#fe5631" name="VIEW ALL APARTMENT"  directto="/" />
      </div>
    </div>
  );
}
