const NewAddedProperties = [
    {
      Image:
        "https://templates.envytheme.com/fido/default/assets/images/new-added-properties/properties-1.jpg",
      For: "RENT",
      Name: "Villa Apartment Peris",
      Address: "64 1st Avenue, High Street, NZ 1002",
      Bedrooms: 4,
      Baths: 2,
      FreeParking: "yes",
      Rating: 4,
      Price: "2,500",
    },
    {
      Image: "https://templates.envytheme.com/fido/default/assets/images/new-added-properties/properties-2.jpg",
      For: "RENT",
      Name: "Villa Apartment Ottawa",
      Address: "64 1st Avenue, High Street, NZ 1002",
      Bedrooms: 4,
      Baths: 2,
      FreeParking: "yes",
      Rating: 3,
      Price: "4,500",
    },
    {
      Image: "	https://templates.envytheme.com/fido/default/assets/images/new-added-properties/properties-3.jpg",
      For: "RENT",
      Name: "Villa Apartment Canberra",
      Address: "64 1st Avenue, High Street, NZ 1002",
      Bedrooms: 3,
      Baths: 2,
      FreeParking: "yes",
      Rating: 5,
      Price: "6,500",
    },
    {
      Image: "https://templates.envytheme.com/fido/default/assets/images/new-added-properties/properties-4.jpg",
      For: "RENT",
      Name: "Villa Apartment Wellington",
      Address: "64 1st Avenue, High Street, NZ 1002",
      Bedrooms: 4,
      Baths: 2,
      FreeParking: "yes",
      Rating: 4,
      Price: "8,500",
    },
  ];
  
  module.exports.NewAddedProperties = NewAddedProperties;