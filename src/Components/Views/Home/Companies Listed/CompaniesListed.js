import React from "react";
import './CompaniesListed.css'
import { logos } from "./companies";

export default function CompaniesListed(){
    return  <div className="companies-container">
    <div className="motto-line">
      <h1>We Only Work With The Best Companies Around The Globe</h1>
    </div>
    <div className="companies-logos">
      {logos.map((logo, index) => {
        return (
          <div key={index} className="company-logo">
            <img src={logo.link} alt={logo.name} />
          </div>
        );
      })}
    </div>
  </div>
}