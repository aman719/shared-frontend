import React from "react";
import Button from "../../../utils/Button/Button"
import "./ContactUs.css";

export default function ContactUs() {
  return (
    <div className="contactus-container">
      <div className="contactus">
        <img
          src="https://templates.envytheme.com/fido/default/assets/images/solution-1.png"
          alt="contactus"
        />
        <div className="descriptionandbtn">
          <h1>Find Best Property Solution For #Rent #Sell and #Buy</h1>
          <br/>
          <p>
            Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin
            lorem quis bibendum auctor nisi elit consequat ipsum nec sagittis
            sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit
            amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio
            tincidunt auctor a ornare odio.
            <br/> <br/>Lorem ipsum dolor sit amet,
            consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat.
          </p>
          <br/>
          <Button width='200px' name="CONTACT US NOW" directto='/' />
        </div>
      </div>
    </div>
  );
}
