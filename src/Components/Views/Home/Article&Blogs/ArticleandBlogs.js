import React, { useLayoutEffect, useState } from "react";
import "./ArticleandBlogs.css";
import { articles } from "./Articles";

export default function ArticleandBlogs() {
  const bodywidth = document.querySelector("body").scrollWidth;

  const [screenwidth, setscreenwidth] = useState(bodywidth);

  useLayoutEffect(() => {
    window.addEventListener("resize", () => {
      setscreenwidth(window.innerWidth);
    });
  }, [screenwidth]);

  const handledot = (e) => {
    
    const dots = e.currentTarget.parentElement.childNodes;

   dots.forEach(dot => {
     dot.style.padding = 0 ;
     dot.style.opacity = 0.5 ;
   });
   e.currentTarget.style.padding = "2px" ;
   e.currentTarget.style.opacity = 1 ;

    if (e.currentTarget.id === "first-dot") {
      document.getElementById("articles").scrollTo(0, 0);
    }
    if(e.currentTarget.id === "second-dot"){
      if((screenwidth < 1150 && screenwidth >= 880)){
        document.getElementById("articles").scrollTo(870, 0);
      }
      if(screenwidth < 880){
        document.getElementById("articles").scrollTo(screenwidth-44.9, 0);
      }
    }
    if(e.currentTarget.id === "third-dot"){
      document.getElementById("articles").scrollTo((screenwidth-44.9)*2,0)
    }
    if(e.currentTarget.id === "fourth-dot"){
      document.getElementById("articles").scrollTo((screenwidth-44.9)*3,0)
    }
    if(e.currentTarget.id === "fifth-dot"){
      document.getElementById("articles").scrollTo((screenwidth-44.9)*4,0)
    }
   
    if (e.currentTarget.id === "last-dot") {
      document.getElementById("articles").scrollTo(1780, 0);
      if (screenwidth > 1450) {
        document.getElementById("articles").scrollTo(1395, 0);
      }
      if (screenwidth < 1450 && screenwidth >= 1150) {
        document.getElementById("articles").scrollTo(1155, 0);
      }
      if (screenwidth < 1150 && screenwidth >= 870) {
        document.getElementById("articles").scrollTo(1780, 0);
      }
      if(screenwidth < 880){
        document.getElementById("articles").scrollTo(screenwidth * dots.length, 0);
      }
    }
  };

  return (
    <div className="articleandblog-container">
      <h1 className="title">Articles and Blogs</h1>
      <p style={{width:screenwidth<=880 ? screenwidth - 30 + "px" :null}} className="description">
        Proin gravida nibh vel velit auctor aliquet aenean sollicitudin lorem
        quis bibendum auctor nisi elit consequat ipsum nec sagittis sem nibh id
        elit.
      </p>
      <div style={{width:screenwidth<=880 ? screenwidth - 75 + "px" :null}} id="articles" className="articles">
        {articles.map((article, index) => {
          return (
            <div style={{width:screenwidth<=880 ? screenwidth - 80 + "px" :null}} key={index} className="article">
              <img
              style={{width:screenwidth<=880 ? screenwidth - 100 + "px" :null}}
                className="article-img"
                src={article.Image}
                alt={article.Title}
              />
              <p className="type">{article.Type}</p>
              <h2 className="heading">{article.Title}</h2>
              <div className="author-and-date" style={{width:screenwidth<=880 ? screenwidth - 80 + "px" :null}}>
                <div className="author">
                  <img
                    className="author-img"
                    src={article.Author.Image}
                    alt={article.Author.Name}
                  />
                  <p className="author-name">&nbsp;{article.Author.Name}</p>
                </div>
                <p className="date">
                  <i className="far fa-calendar-alt"></i>&nbsp;{article.Date}
                </p>
              </div>
            </div>
          );
        })}
      </div>
      <br />
      <div className="dots">
        <i onClick={handledot} id="first-dot" className="fas fa-circle"></i>
       
        <i
          style={{
            display: screenwidth < 1150 ? "" : "none",
            opacity: 0.5,
          }}
          onClick={handledot}
          id="second-dot"
          className="fas fa-circle"
        ></i>
        <i
          style={{
            display: screenwidth < 880 ? "" : "none",
            opacity: 0.5,
          }}
          onClick={handledot}
          id="third-dot"
          className="fas fa-circle"
        ></i>
        <i
          style={{
            display: screenwidth < 880 ? "" : "none",
            opacity: 0.5,
          }}
          onClick={handledot}
          id="fourth-dot"
          className="fas fa-circle"
        ></i>
        <i
          style={{
            display: screenwidth < 880 ? "" : "none",
            opacity: 0.5,
          }}
          onClick={handledot}
          id="fifth-dot"
          className="fas fa-circle"
        ></i>
        <i onClick={handledot} id="last-dot" className="fas fa-circle"></i>
      </div>
    </div>
  );
}
