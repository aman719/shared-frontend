const articles = [
    {
        Image: "https://templates.envytheme.com/fido/default/assets/images/blog/blog-1.jpg",
        
        Type : "APARTMENT",
        Title : "Redfin Unveils the Best Canadian Cities for Biking",
        Author : {
            Name : "Alex Morgan",
            Image : "https://templates.envytheme.com/fido/default/assets/images/blog/image-1.jpg"
        },
        Date : "February 24, 2021"
    },
    {
        Image: "https://templates.envytheme.com/fido/default/assets/images/blog/blog-2.jpg",
        Type : "HOUSES",
        Title : "Apartio Helps Get Your Dream & Luxury Space Alexa",
        Author : {
            Name : "Nia Matos",
            Image : "https://templates.envytheme.com/fido/default/assets/images/blog/image-2.jpg"
        },
        Date : "January 4, 2021"
    },
    {
        Image: "https://templates.envytheme.com/fido/default/assets/images/blog/blog-3.jpg",
        Type : "COMMERCIAL",
        Title : "Housing Markets That Changed the Most This Decade",
        Author : {
            Name : "Milana Myles",
            Image : "https://templates.envytheme.com/fido/default/assets/images/blog/image-3.jpg"
        },
        Date : "June 2, 2021"
    },
    {
        Image: "https://templates.envytheme.com/fido/default/assets/images/blog/blog-2.jpg",
        Type : "HOUSES",
        Title : "Apartio Helps Get Your Dream & Luxury Space Alexa",
        Author : {
            Name : "Nia Matos",
            Image : "https://templates.envytheme.com/fido/default/assets/images/blog/image-2.jpg"
        },
        Date : "January 4, 2021"
    },
    {
        Image: "https://templates.envytheme.com/fido/default/assets/images/blog/blog-3.jpg",
        Type : "COMMERCIAL",
        Title : "Housing Markets That Changed the Most This Decade",
        Author : {
            Name : "Milana Myles",
            Image : "https://templates.envytheme.com/fido/default/assets/images/blog/image-3.jpg"
        },
        Date : "June 2, 2021"
    },
    {
        Image: "https://templates.envytheme.com/fido/default/assets/images/blog/blog-1.jpg",
        
        Type : "APARTMENT",
        Title : "Redfin Unveils the Best Canadian Cities for Biking",
        Author : {
            Name : "Alex Morgan",
            Image : "https://templates.envytheme.com/fido/default/assets/images/blog/image-1.jpg"
        },
        Date : "February 24, 2021"
    },
]

module.exports.articles = articles;