import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { gallerypageview } from "../../../../_actions/gallery_action";
import LoadingIcon from "../../../utils/LoadingIcon/LoadingIcon";
import PageBanner from "../../../utils/PageBanner/PageBanner";

function SP_Gallery() {
  const dispatch = useDispatch();
  const [Gallery, setGallery] = useState([]);
  const [ShowLoading, setShowLoading] = useState(true);

  useEffect(() => {
    dispatch(gallerypageview()).then((response) => {
      if (response.payload.success) {
        // console.log("response", response);
        setGallery(response.payload.projects);
        // console.log(response.payload.projects);
        setShowLoading(false);
      } else {
        alert("Failed");
      }
    });
  }, []);

  const OpenImageDetails = (Image) => {
    // console.log("Image Address ", Image);
  };

  return (
    <div>
      {ShowLoading ? (
        <LoadingIcon />
      ) : (
        <div>
          <PageBanner
            title="Gallery"
          />

          {Gallery.map((gallery, index) => {
            return (
              <>
                <div>{gallery._id}</div>
                <div>
                  {gallery.Images.map((arrayindex, value) => {
                    return (
                      <div onClick={() => OpenImageDetails(arrayindex)}>
                        <img key={value} src={arrayindex} width={200} />
                      </div>
                    );
                  })}
                </div>
              </>
            );
          })}
        </div>
      )}
    </div>
  );
}

export default SP_Gallery;
