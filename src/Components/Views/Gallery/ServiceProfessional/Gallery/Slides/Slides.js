import React, { useEffect, useLayoutEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { gallerypageview } from "../../../../../../_actions/gallery_action";
import { Get_ProjectDetails_By_ProjectID } from "../../../../../../_actions/project_actions";
// import { slides } from "./Data";
import "./Slides.css";

export default function Slides(props) {
  // console.log("props.galleryimages", props.galleryimages);
  const company = useSelector(state => state.company);
  const dispatch = useDispatch();

  const [windowwidth, setwindowwidth] = useState(String);
  const [windowheight, setwindowheight] = useState(String);
  const [slideimgwidth, setslideimgwidth] = useState(String);
  const [slideimgheight, setslideimgheight] = useState(String);
  const [detailwidth, setdetailwidth] = useState(String);
  const [detailheight, setdetailheight] = useState(String);
  const bodywidth = document.querySelector("body").scrollWidth;
  const [screenwidth, setscreenwidth] = useState(bodywidth);
  const [ProjectDetails, setProjectDetails] = useState([]);
  const [ServiceProfessionalAddress, setServiceProfessionalAddress] = useState([]);
  const [ServiceProfessionalNameDetails, setServiceProfessionalNameDetails] = useState([]);

  const [imgdetailindex, setimgdetailindex] = useState(0);

  const galleryimages = props.galleryimages;

  const currentslide = galleryimages.filter(
    (image) => image.Id === props.imgid
  )[0];

  useLayoutEffect(() => {
    window.addEventListener("resize", () => {
      setscreenwidth(window.innerWidth);
    });
  }, [screenwidth]);

  useEffect(() => {

    dispatch(Get_ProjectDetails_By_ProjectID(props.imgid))
    .then((response) => {
      if(response){
        // console.log("Image Details Response", response);
        setProjectDetails(response.payload.doc[0]);
        setServiceProfessionalNameDetails(response.payload.doc[0].WriterDetails);
        setServiceProfessionalAddress(response.payload.doc[0].Address);
      }else {
        // console.log("Failed To fetch details");
      }
    })

    setwindowheight(
      document.querySelector("body").getBoundingClientRect().height
    );
    setwindowwidth(
      document.querySelector("body").getBoundingClientRect().width + "px"
    );
    setslideimgwidth(
      document.getElementById("slide-img").getBoundingClientRect().width
    );
    setslideimgheight(
      document.getElementById("slide-img").getBoundingClientRect().height
    );
    setdetailwidth(
      document.getElementById("detail").getBoundingClientRect().width
    );
    setdetailheight(
      document.getElementById("detail").getBoundingClientRect().height
    );
  }, [screenwidth]);

  const handlearrows = (e) => {
    const currentelement = e.currentTarget.className;
    const imageslength =
      e.currentTarget.parentElement.nextSibling.childNodes[0].childNodes
        .length - 1;

    const slidetransformwidth = parseFloat(
      document
        .getElementById("slide-images")
        .style.transform.replace(/[^\d.]/g, "")
    );
    const detailtransformwidth = parseInt(
      document.getElementById("details").style.transform.replace(/[^\d.]/g, "")
    );

    if (currentelement === "fas fa-chevron-left") {
      if (
        slidetransformwidth.toString() === "NaN" ||
        parseInt(slidetransformwidth) === 0
      ) {
        document.getElementById(
          "slide-images"
        ).style.transform = `translateX(-${slideimgwidth * imageslength}px)`;
        setimgdetailindex(imageslength);
        // document.getElementById("details").style.transform = `translateX(-${
        //   detailwidth * imageslength
        // }px)`;
      } else {
        document.getElementById(
          "slide-images"
        ).style.transform = `translateX(-${Math.abs(
          slidetransformwidth - slideimgwidth
        )}px)`;
        // document.getElementById(
        //   "details"
        // ).style.transform = `translateX(-${Math.abs(
        //   parseInt(detailtransformwidth - detailwidth)
        // )}px)`;
        setimgdetailindex(imgdetailindex - 1);
      }
    }
    if (currentelement === "fas fa-chevron-right") {
      if (
        slidetransformwidth.toString() === "NaN" ||
        slidetransformwidth === 0
      ) {
        document.getElementById(
          "slide-images"
        ).style.transform = `translateX(-${slideimgwidth}px)`;
        setimgdetailindex(imgdetailindex + 1);
        // document.getElementById(
        //   "details"
        // ).style.transform = `translateX(-${detailwidth}px)`;
      }
      if (
        parseInt(slidetransformwidth) >= parseInt(slideimgwidth * imageslength)
      ) {
        document.getElementById(
          "slide-images"
        ).style.transform = `translateX(-${0}px)`;
        // document.getElementById(
        //   "details"
        // ).style.transform = `translateX(-${0}px)`;
        setimgdetailindex(0);
      } else {
        document.getElementById(
          "slide-images"
        ).style.transform = `translateX(-${
          slideimgwidth + slidetransformwidth
        }px)`;
        // document.getElementById("details").style.transform = `translateX(-${
        //   detailwidth + detailtransformwidth
        // }px)`;
        setimgdetailindex(imgdetailindex + 1);
      }
    }
  };

  const handlezoom = (e) => {
    const images = document.getElementById("slide-images").childNodes;
    const transformwidth = parseInt(
      document
        .getElementById("slide-images")
        .style.transform.replace(/[^\d.]/g, "")
    );
    const currentimg = Math.floor(transformwidth / parseInt(slideimgwidth));
    const clickedbtn = e.currentTarget.id;
    if (clickedbtn === "zoom-in") {
      for (let i = 0; i < images.length; i++) {
        images[i].style.transform = "scale(1)";
      }
      if (transformwidth.toString() === "NaN") {
        images[0].style.transform = "scale(1.4)";
      } else {
        images[currentimg].style.transform = "scale(1.4)";
      }
    } else {
      for (let i = 0; i < images.length; i++) {
        images[i].style.transform = "scale(1)";
      }
    }
  };

  const handleclose = (e) => {
    document.getElementById("slides-and-details-container").style.opacity = 0;
    document.getElementById(
      "slides-and-details-container"
    ).style.pointerEvents = "none";
  };

  function companyDetials(ImageID){
    // console.log("Image ID ", ImageID);
    
    return "Hello";
  }

  return (
    <div
      style={{
        width: windowwidth,
        height: windowheight + "px",
        pointerEvents: props.pointerEvents,
      }}
      id="slides-and-details-container"
      className="slides-and-details-container"
    >
      <div id="slides-container" className="slides-container">
        <div className="slides-upper">
          <div onClick={handleclose} className="close-icon">
            <i className="material-icons">close</i>
          </div>
          <div className="zoom">
            <i onClick={handlezoom} id="zoom-in" className="material-icons">
              zoom_in
            </i>
            <i onClick={handlezoom} id="zoom-out" className="material-icons">
              zoom_out
            </i>
          </div>
        </div>
        <div className="arrow-and-slides">
          {/* <div className="arrows">
            <i onClick={handlearrows} className="fas fa-chevron-left"></i>
            <i onClick={handlearrows} className="fas fa-chevron-right"></i>
          </div> */}
          <div
            style={{
              width: screenwidth <= 670 ? screenwidth / 1.05 + "px" : null,
            }}
            className="slides"
          >
            <div id="slide-images" className="slide-images">
              {/* {currentslide.Images.map((img, index) => { */}
              {/* return ( */}
              <img id="slide-img" src={currentslide.Image} alt="img" />
              {/* ); */}
              {/* })} */}
            </div>
          </div>
        </div>
      </div>
      <div id="img-details-container" className="img-details-container">
        <h2 className="heading">Image Details</h2>
        <div className="details-container">
          <div id="details" className="details">
            <div
              style={{
                maxHeight: detailheight + "px",
              }}
              className="detail-list"
            >
              <div id="detail" className="detail">

                <p className="title">Service Professional Profile ID :</p>
                {company.companyData && company.companyData.isAuth ? 
                  <a href={`/ServiceProfessional/Profile/OtherView/${ProjectDetails.writer}`} style={{ textDecoration: 'none', color: '#1F80E0' }}>
                    View Info
                  </a>
                  : 
                <a href={`/AllServiceProfessionalPage/Profile/OtherView/${ProjectDetails.writer}`} style={{ textDecoration: 'none', color: '#1F80E0' }}>
                  View Info
                </a>
                }
              
                <p className="title">Service Professional Name :</p>
                {company.companyData && company.companyData.isAuth ? 
                  <a href={`/ServiceProfessional/Profile/OtherView/${ProjectDetails.writer}`} style={{ textDecoration: 'none', color: '#1F80E0' }}>
                  {ServiceProfessionalNameDetails.Service_Professional_Name}
                  </a>
                :
                  <a href={`/AllServiceProfessionalPage/Profile/OtherView/${ProjectDetails.writer}`} style={{ textDecoration: 'none', color: '#1F80E0' }}>
                    {ServiceProfessionalNameDetails.Service_Professional_Name}
                  </a>
                }
              
                <p className="title">Company Name :</p>
                {company.companyData && company.companyData.isAuth ? 
                  <a href={`/ServiceProfessional/Profile/OtherView/${ProjectDetails.writer}`} style={{ textDecoration: 'none', color: '#1F80E0' }}>
                    {ServiceProfessionalNameDetails.Company_Name}
                  </a>
                  :
                  <a href={`/AllServiceProfessionalPage/Profile/OtherView/${ProjectDetails.writer}`} style={{ textDecoration: 'none', color: '#1F80E0' }}>
                    {ServiceProfessionalNameDetails.Company_Name}
                  </a>
                }
              
                <p className="title">Project Number :</p>
                <a href={`/ProjectsDetails/${ProjectDetails._id}`} style={{ textDecoration: 'none', color: '#1F80E0' }}>View Info</a>

                <p className="title">Project Name :</p>
                <p>{ProjectDetails.Project_Name}</p>
              

                <p className="title">Project Client Name :</p>
                <p>{ProjectDetails.Project_Client_Name}</p>
              

                <p className="title">Project Year of Completion :</p>
                <p>{ProjectDetails.Project_Completion}</p>

                <p className="title">Project Page link to Company Website / Company YouTube Channel URL :</p>
                <a href={ProjectDetails.Project_Page_Link} target="_blank" style={{ textDecoration: 'none', color: '#1F80E0' }}>{ProjectDetails.Project_Page_Link}</a>
                
                <p className="title">Project Service Requirement :</p>
                <p>{ProjectDetails.Service_Requirement}</p>
                
                <p className="title">Project Type & Style :</p>
                <p>{ProjectDetails.Project_Type} - {ProjectDetails.Project_Type_Subcategory_1} - {ProjectDetails.Project_Type_Subcategory_2} - {ProjectDetails.Project_Architecture_Style}</p>

                <p className="title">Image Title :</p>
                <p>{currentslide.Title}</p>
              
                <p className="title">Image Category :</p>
                <div className="categories">
                {currentslide.Category.map((item) => {
                  return (
                  <p className="category">{item}</p>
                  )
                })}
                </div>
                <p className="title">Image Description :</p>
                <p>
                  {currentslide.Description}
                </p>
                {/* <p className="title">Company Name</p>
                <p>
                  {companyDetials(currentslide.Image)}
                </p> */}
              </div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
