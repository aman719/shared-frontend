const Plans = [
  {
    Name: "Weby Bear",
    Price: "₹ 140 per lead",
    Validity: "1 Month",
    Features: [
      "Plan Validity: 1 month",
      "Add unlimited Branches",
      "Upload unlimited Projects",
      "Upload unlimited High Quality Images",
      "Select 35 (28 Leads + 7 Extra Leads)",
      "1 Lead is shared with 3 Service Professionals",
      "Gain access to Profile Performance & Analytics",
      "Contact additional unlimited clients using Analytics",
      "Featured Profile in relevant cities & categories for 1 month",
      "Featured Projects in relevant cities & categories for 1 month",
    ],
  },
  {
    Name: "Weby Tiger",
    Price: "₹ 300 per lead",
    Validity: "1 Month",
    Features: [
      "Plan Validity: 1 month",
      "Add unlimited Branches",
      "Upload unlimited Projects",
      "Upload unlimited High Quality Images",
      "Select 17 (13 Leads + 4 Extra Leads)",
      "1 Lead is shared with 1 Service Professional",
      "Gain access to Profile Performance & Analytics",
      "Contact additional unlimited clients using Analytics",
      "Featured Profile in relevant cities & categories for 1 month",
      "Featured Projects in relevant cities & categories for 1 month",
    ],
  }
];

const BabyFeatures = [
  "Plan Validity: 1 month",
  "Add unlimited Branches",
  "Upload unlimited Projects",
  "Upload unlimited High Quality Images",
  "Contact unlimited Clients",
  "Select unlimited Property Requirements",
  "Gain access to Profile Performance & Analytics",
  "Contact additional unlimited clients using Analytics",
  "Featured Profile in relevant cities & categories for 1 month",
  "Featured Projects in relevant cities & categories for 1 month",
];

const PapaFeatures = [
  "Plan Validity: 3 months",
  "Add unlimited Branches",
  "Upload unlimited Projects",
  "Upload unlimited High Quality Images",
  "Contact unlimited Clients",
  "Select unlimited Property Requirements",
  "Gain access to Profile Performance & Analytics",
  "Contact additional unlimited clients using Analytics",
  "Featured Profile in relevant cities & categories for 3 months",
  "Featured Projects in relevant cities & categories for 3 months",
];

const MamaFeatures = [
  "Plan Validity: 6 months",
  "Add unlimited Branches",
  "Upload unlimited Projects",
  "Upload unlimited High Quality Images",
  "Contact unlimited Clients",
  "Select unlimited Property Requirements",
  "Gain access to Profile Performance & Analytics",
  "Contact additional unlimited clients using Analytics",
  "Featured Profile in relevant cities & categories for 6 months",
  "Featured Projects in relevant cities & categories for 6 months",
];

const TeddyFeatures = [
  "Plan Validity: 12 months",
  "Add unlimited Branches",
  "Upload unlimited Projects",
  "Upload unlimited High Quality Images",
  "Contact unlimited Clients",
  "Select unlimited Property Requirements",
  "Gain access to Profile Performance & Analytics",
  "Contact additional unlimited clients using Analytics",
  "Featured Profile in relevant cities & categories for 12 months",
  "Featured Projects in relevant cities & categories for 12 months",
];

module.exports.Plans = Plans;
module.exports.BabyFeatures = BabyFeatures;
module.exports.PapaFeatures = PapaFeatures;;
module.exports.MamaFeatures = MamaFeatures;
module.exports.TeddyFeatures = TeddyFeatures;
