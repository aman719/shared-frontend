import React from "react";
import Razorpay from "../../../utils/Razorpay/SP_subscription/Razorpay";
import "./PlanBreakDown.css";
import { BABY_BEAR_PRICE, MAMA_BEAR_PRICE, PAPA_BEAR_PRICE, TEDDY_BEAR_PRICE, WEBY_BEAR_PRICE, WEBY_TIGER_PRICE } from "../../../../PricingValidityConfig";

const PlanBreakDown = (props) => {
  const plan = props.plan;

  return (
    <div style={props.styles} className="plan-breakdown-container">
      <div className="plan-breakdown">
        <span onClick={props.handleclose} id="close" className="material-icons">
          cancel
        </span>
        <div className="plan-breakdown-details">
          <div className="plan-title-and-price">
            <p className="title">{plan.Name}</p>
            <p className="pricing">{plan.Price}</p>
          </div>
          <p className="features-title">Features :</p>
          <div className="features-container">
            <div className="features">
              {plan.Features.map((feature, index) => {
                return (
                  <p key={index} className="feature">
                    <span id="dot" class="material-icons">
                      fiber_manual_record
                    </span>
                    {feature}
                  </p>
                );
              })}
            </div>
            <div className="billing-details">
              <div className="fee-and-taxes">
                <h3 style={{ textAlign: 'center' }}>Amount Per Month</h3>
                <div className="fee">
                  <p className="title">Cost Per Lead:</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? 175 : plan.Name === "Baby Tiger" ? 375 : plan.Name === "Weby Bear" ? 140 : plan.Name === "Weby Tiger" ? 300 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">Total No. of Leads:</p>
                  <p className="amount">{plan.Name === "Baby Bear" ? 35 : plan.Name === "Baby Tiger" ? 17 : plan.Name === "Weby Bear" ? 35 : plan.Name === "Weby Tiger" ? 17 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">Platform Fee:</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? 100 : plan.Name === "Baby Tiger" ? 124 : plan.Name === "Weby Bear" ? 79 : plan.Name === "Weby Tiger" ? 99 : 0}</p>
                </div>
                
                <br />
                <h3 style={{ textAlign: 'center' }}>No. of Months</h3>
                <div className="fee">
                  <p className="title">No. of Months:</p>
                  <p className="amount">{plan.Name === "Baby Bear" ? 1 : plan.Name === "Papa Bear" ? 3 : plan.Name === "Mama Bear" ? 6 : plan.Name === "Teddy Bear" ? 12 : plan.Name === "Baby Tiger" ? 1 : plan.Name === "Weby Bear" ? 1 : plan.Name === "Weby Tiger" ? 1 : 0}</p>
                </div>
                <br />
                <h3 style={{ textAlign: 'center' }}>Amount To Be Paid</h3>
                <div className="fee">
                  <p className="title">Amount (Exclusive of G.S.T.) * {plan.Name === "Baby Bear" ? 1 : plan.Name === "Papa Bear" ? 3 : plan.Name === "Mama Bear" ? 6 : plan.Name === "Teddy Bear" ? 12 : plan.Name === "Baby Tiger" ? 1 : plan.Name === "Weby Bear" ? 1 : plan.Name === "Weby Tiger" ? 1 : 0}</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? BABY_BEAR_PRICE : plan.Name === "Papa Bear" ? PAPA_BEAR_PRICE : plan.Name === "Mama Bear" ? MAMA_BEAR_PRICE : plan.Name === "Teddy Bear" ? TEDDY_BEAR_PRICE : plan.Name === "Baby Tiger" ? 4999 : plan.Name === "Weby Bear" ? WEBY_BEAR_PRICE : plan.Name === "Weby Tiger" ? WEBY_TIGER_PRICE : 0}</p>
                </div>
                <br />
              </div>
              <br />
              <button className="proceed">
                <Razorpay
                  Name={plan.Name}
                  Price={plan.Price}
                  Validity={plan.Validity}
                />
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PlanBreakDown;
