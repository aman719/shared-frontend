import React, { useLayoutEffect, useState } from "react";
import "./FeatureAgents.css";
import { featureagents } from "./Data";
import Button from "../../../utils/Button/Button";

export default function FeatureAgents() {
  const bodywidth = document.querySelector("body").scrollWidth;

  const [screenwidth, setscreenwidth] = useState(bodywidth);

  useLayoutEffect(() => {
    window.addEventListener("resize", () => {
      setscreenwidth(window.innerWidth);
      // console.log(screenwidth);
    });
  }, [screenwidth]);

  const handlearrow = (e) => {
    const currentelement = e.currentTarget.className;
    var widthtomove = 1385;
    var nodes =
      (document.getElementById("feature-agents").childNodes.length - 3) / 3;
    if (screenwidth <= 1450) {
      widthtomove = 1185;
    }
    if (screenwidth <= 1250) {
      widthtomove = 985;
    }
    if (screenwidth <= 1050) {
      widthtomove = 850;
      nodes =
        (document.getElementById("feature-agents").childNodes.length - 2) / 2;
    }
    if (screenwidth <= 900) {
      widthtomove = 675;
    }
    if (screenwidth <= 900) {
      widthtomove = 502;
      nodes =
        (document.getElementById("feature-agents").childNodes.length - 1)
    }
    if(screenwidth <= 530){
      widthtomove = screenwidth - 28;
    }
    const transformwidth = parseInt(
      document
        .getElementById("feature-agents")
        .style.transform.replace(/[^\d.]/g, "")
    );

    if (currentelement === "fas fa-arrow-left") {
      if (transformwidth.toString() === "NaN" || transformwidth === 0) {
        document.getElementById(
          "feature-agents"
        ).style.transform = `translateX(-${widthtomove * nodes}px)`;
      } else {
        document.getElementById(
          "feature-agents"
        ).style.transform = `translateX(-${transformwidth - widthtomove}px)`;
      }
    }
    if (currentelement === "fas fa-arrow-right") {
      if (transformwidth.toString() === "NaN" || transformwidth === 0) {
        document.getElementById(
          "feature-agents"
        ).style.transform = `translateX(-${widthtomove}px)`;
      }
      if (transformwidth >= widthtomove * nodes) {
        document.getElementById(
          "feature-agents"
        ).style.transform = `translateX(-${0}px)`;
      } else {
        document.getElementById(
          "feature-agents"
        ).style.transform = `translateX(-${widthtomove + transformwidth}px)`;
      }
    }
  };

  return (
    <div className="feature-agents-container">
      <p className="title">
        <b>Explore Feature Agents</b>
      </p>
      <p style={{ width: screenwidth <= 530 ? screenwidth - 30 + "px" : null }} className="description">
        Proin gravida nibh vel velit auctor aliquet aenean sollicitudin lorem
        quis bibendum auctor nisi elit consequat ipsum nec sagittis sem nibh id
        elit.
      </p>
      <div
        style={{ width: screenwidth <= 530 ? screenwidth - 20 + "px" : null }}
        className="feature-agents"
      >
        <div className="arrows">
          <i onClick={handlearrow} id="left" className="fas fa-arrow-left"></i>
          <i
            onClick={handlearrow}
            id="right"
            className="fas fa-arrow-right"
          ></i>
        </div>
        <div id="feature-agents" className="agents">
          {featureagents.map((agent, index) => {
            return (
              <div
              style={{ width: screenwidth <= 530 ? screenwidth - 40 + "px" : null }}
                className="agent"
                key={index}
              >
                <div className="upper">
                  <div style={{
            height : screenwidth <= 530 ? screenwidth-100 + 'px' : null}} className="imgandicons">
                    <img style={{ width: screenwidth <= 530 ? screenwidth - 65 + "px" : null }} src={agent.Image} alt={agent.Name} />
                    <div className="social-icons">
                      <a href={`${agent.SocialHandles.fb}`}>
                        <i id="fb" className="fab fa-facebook-f"></i>
                      </a>
                      <a href={`${agent.SocialHandles.insta}`}>
                        <i id="insta" className="fab fa-instagram"></i>
                      </a>
                      <a href={`${agent.SocialHandles.twitter}`}>
                        <i id="twt" className="fab fa-twitter"></i>
                      </a>
                      <a href={`${agent.SocialHandles.pinterest}`}>
                        <i id="pin" className="fab fa-pinterest-p"></i>
                      </a>
                    </div>
                  </div>
                  <div className="details">
                    <div className="addressandmail">
                      <p className="address">{agent.Address}</p>
                      <a href={`mailto:${agent.Email}`}>
                        <i id="mail-icon" className="far fa-envelope"></i>
                      </a>
                    </div>
                    <p className="name">
                      <b>{agent.Name}</b>
                    </p>
                    <p className="properties">
                      {agent.Properties}&nbsp;Properties
                    </p>
                    <div className="rating">
                      <i className="fas fa-star"></i>
                      <i className="fas fa-star"></i>
                      <i className="fas fa-star"></i>
                      <i className="fas fa-star"></i>
                      <i className="fas fa-star"></i>
                      <p>Average</p>
                    </div>
                  </div>
                </div>
                <div className="telandbtn">
                  <a className="tel" href={`tel:${agent.PhNumber}`}>
                    <i className="fas fa-phone-alt"></i>
                    {agent.PhNumber}
                  </a>
                  <div className="btn">
                    <Button width="160px" background="#201c2d" name="KNOW DETAILS" />
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      <br />
      <div>
        <Button  name="VIEW ALL AGENTS" width="200px" directto='/agents' />
      </div>
    </div>
  );
}
