import React, { useLayoutEffect, useState } from "react";
import PageBanner from "../../../utils/PageBanner/PageBanner";
import "./ServiceDetails.css";

export default function ServiceDetails() {
  const bodywidth = document.querySelector("body").scrollWidth;

  const [screenwidth, setscreenwidth] = useState(bodywidth);

  useLayoutEffect(() => {
    window.addEventListener("resize", () => {
      setscreenwidth(window.innerWidth);
    });
  }, [screenwidth]);
  return (
    <div className="service-details-container">
      <PageBanner
        title="Services Details"
      />
      <div className="service-details">
        <div style={{
          width:screenwidth <= 1000 ? screenwidth-50 + 'px' : null
        }} className="details">
          <div className="first">
            <div className="titleanddescription">
              <h2 className="title">Strategic & Commercial Approach</h2>
              <p className="description">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged.
                Randomised words which don't slightly believable. There are many
                variations of passages of Lorem Ipsum available, but the
                majority have suffered alteration in some form, by injected
                humour, or randomised words which don't look even slightly
                believable. There are many variations of passages of Lorem Ipsum
                available, but the majority have suffered alteration in some
                for.
              </p>
            </div>
            <div className="image">
              <img
              style={{
                width:screenwidth <= 1000 ? screenwidth-50 + 'px' : null,
                height : screenwidth <= 1000 ? screenwidth-200 + 'px' : null
              }}
                src="https://templates.envytheme.com/fido/default/assets/images/services-details/services-details-1.jpg"
                alt="img"
              />
            </div>
          </div>
          <br />
          <div className="second">
            <h2 className="title">What Benefit You Will Get</h2>
            <p className="description">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in sed quia non numquam eius modi tempora incidunt
              ut labore et dolore magnam aliquam quaerat voluptatem.
            </p>
            <div className="benefits">
              <ul style={{width:screenwidth <= 1000 ? screenwidth-50 : null}}>
                <li>
                  <i className="fas fa-check"></i>&nbsp;&nbsp;Scientific Skills
                  For Getting a Better Result
                </li>
                <li>
                  <i className="fas fa-check"></i>&nbsp;&nbsp;Scientific Skills
                  For Getting a Better Result
                </li>
                <li>
                  <i className="fas fa-check"></i>&nbsp;&nbsp;Scientific Skills
                  For Getting a Better Result
                </li>
                <li>
                  <i className="fas fa-check"></i>&nbsp;&nbsp;Scientific Skills
                  For Getting a Better Result
                </li>
                <li>
                  <i className="fas fa-check"></i>&nbsp;&nbsp;Scientific Skills
                  For Getting a Better Result
                </li>
                <li>
                  <i className="fas fa-check"></i>&nbsp;&nbsp;Scientific Skills
                  For Getting a Better Result
                </li>
                <li>
                  <i className="fas fa-check"></i>&nbsp;&nbsp;Scientific Skills
                  For Getting a Better Result
                </li>
                <li>
                  <i className="fas fa-check"></i>&nbsp;&nbsp;Scientific Skills
                  For Getting a Better Result
                </li>
              </ul>
            </div>
          </div>
          <br />
          <div className="third">
            <h2 className="title">Ready Flat For Rent</h2>
            <p className="description">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in sed quia non numquam eius modi tempora incidunt
              ut labore et dolore magnam aliquam quaerat voluptatem.
            </p>
            <div className="images">
              <img
              style={{
                width:screenwidth <= 1000 ? screenwidth-50 + 'px' : null,
                height : screenwidth <= 1000 ? screenwidth-200 + 'px' : null
              }}
                src="https://templates.envytheme.com/fido/default/assets/images/services-details/services-details-2.jpg"
                alt="img"
              />
              <img
              style={{
                width:screenwidth <= 1000 ? screenwidth-50 + 'px' : null,
                height : screenwidth <= 1000 ? screenwidth-200 + 'px' : null
              }}
                src="https://templates.envytheme.com/fido/default/assets/images/services-details/services-details-3.jpg"
                alt="img"
              />
              <img
              style={{
                width:screenwidth <= 1000 ? screenwidth-50 + 'px' : null,
                height : screenwidth <= 1000 ? screenwidth-200 + 'px' : null
              }}
                src="https://templates.envytheme.com/fido/default/assets/images/services-details/services-details-4.jpg"
                alt="img"
              />
            </div>
            <p className="description">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in sed quia non numquam eius modi tempora incidunt
              ut labore et dolore magnam aliquam quaerat voluptatem. Lorem ipsum
              dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
              minim veniam, quis nostrud exercitation ullamco laboris nisi ut
              aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in sed quia non numquam eius modi tempora incidunt
              ut labore et dolore magnam aliquam quaerat voluptatem.
            </p>
          </div>
        </div>
      </div>
      <br/><br/>
    </div>
  );
}
