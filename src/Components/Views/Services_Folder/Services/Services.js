import React, { useEffect, useLayoutEffect, useState } from "react";
import PageBanner from "../../../utils/PageBanner/PageBanner";
import "./Services.css";
import { services } from "./Data";
import NavigationNumbers from "../../../utils/NavigationNumbers/NavigationNumbers"

export default function Services() {
  const bodywidth = document.querySelector("body").scrollWidth;
  const [clientHeight , setclientHeight] = useState(Number)

  const [screenwidth, setscreenwidth] = useState(bodywidth);

  useLayoutEffect(() => {
    window.addEventListener("resize", () => {
      setscreenwidth(window.innerWidth);
      
    });
  }, [screenwidth]);
 
  


  return (
    <div className="services-container">
      <PageBanner
        title="Services"
      />
      <div 
      style={{
        height:(clientHeight * 3)+60 + 'px'     
      }}
      className="all-services">
        <div id="allservices" className="services">
          {services.map((service, index) => {
            return (
              <div
              style={{
                width:screenwidth<=500 ? screenwidth-50 + 'px' : null,
                height : "auto"
              }}
              id="Service"
              key={index} className="service">
                <div>
                  <i className="fas fa-warehouse"></i>
                  <a href="/">{service.Title}</a>
                  <p className="description">{service.Description}</p>
                </div>
                <span></span>
              </div>
            );
          })}
        </div>
      </div>
      <br />
      <NavigationNumbers
        numberofitems={9}
        screenwidthfornav1={1120}
        numberofitems1={6}
        screenwidthfornav2={770}
        numberofitems2={4}
        itemscontainerid="allservices"
        heighttomove={(clientHeight * 3) + 60}
      />
     

      <br />
    </div>
  );
}