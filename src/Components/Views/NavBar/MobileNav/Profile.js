import React from "react";
import "./MobileNav.css";

export default function Profile(props) {
  document.querySelector("html").addEventListener("click", (e) => {
    const userdetails = document.getElementById(
      "mobile-nav-user-detail"
    ).childNodes;

    const contains1 = document
      .getElementById("mobile-nav-user")
      .contains(e.target);
    const contains2 = document
      .getElementById("add-listing-btn")
      .contains(e.target);
    // ---

    if (!contains1) {
      for (let i = 0; i < userdetails.length; i++) {
        userdetails[i].style.opacity = 0;
        userdetails[i].style.visibility = "hidden";
      }
    }
    if (!contains2) {
      document.getElementById("add-listing").style.opacity = 0;
      document.getElementById("add-listing").style.visibility = "hidden";
    }
  });
  return (
    <div id="mobile-nav-user" className="mobile-nav-user">
      <span onClick={props.handleuser} className="material-icons">
        person
      </span>
      <div id="mobile-nav-user-detail" className="mobile-nav-user-detail">
        {/* Logged In */}
        {/* <ul style={{
        opacity:0
      }}  className="profile-items-drop-down">
     <li><a href="/profile" >View Profile</a></li>
     <li><a href="/account" >Account</a></li>
     <li><a href="/logout" >Log Out</a></li>
   </ul> */}
        {/* Not Logged In */}
        <ul
          style={{
            opacity: 0,
          }}
          className="mobile-nav-login-dropdown"
        >
          <li>
            <p>Login</p>
            <ul>
              <li>
                <a href="/PropertyOwner/Login">Property Owner</a>
              </li>
              <li>
                <a href="/ServiceProfessional/Login">Company</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  );
}
