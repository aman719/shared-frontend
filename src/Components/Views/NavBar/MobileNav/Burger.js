import React from "react";
import './MobileNav.css'

export default function  Burger(props){
    return    <div onClick={props.handleburger} className="burger">
    <span id="open-navbar-icon" className="material-icons">menu</span>
  </div>
}