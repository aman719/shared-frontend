import React, { useEffect, useLayoutEffect, useState } from "react";
import PageBanner from "../../../../utils/PageBanner/PageBanner";
import "./PostedPropertyDetails.css";
import { useDispatch, useSelector } from "react-redux";
import { productDetailsbyID } from "../../../../../_actions/property_actions";
import LoadingIcon from "../../../../utils/LoadingIcon/LoadingIcon";

export default function PostedPropertyDetails(props) {
  const bodywidth = document.querySelector("body").scrollWidth;

  const [screenwidth, setscreenwidth] = useState(bodywidth);

  useLayoutEffect(() => {
    window.addEventListener("resize", () => {
      setscreenwidth(window.innerWidth);
      // console.log(screenwidth);
    });
  }, [screenwidth]);
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [ProductDetails, setProductDetails] = useState([]);

  const productId = props.match.params.PostedPropertyId;
  // console.log("productId =",productId);

  useEffect(() => {
    dispatch(productDetailsbyID(productId)).then((response) => {
      // console.log("Response", response);
      setProductDetails(response.payload);
    });
  }, []);

  if (user.userData && user.userData.isAuth) {
    let userDetails = user.userData;
    return (
      <div className="compare-property-container">
        <PageBanner
          title="Posted Property Requirement Details"
        />
        <div className="compare-property">
        <div className="header">
        <p className="title">
          <b>Posted Property Requirement Details</b>
        </p>
        <p
          style={{ width: screenwidth <= 650 ? screenwidth - 30 + "px" : null }}
          className="description"
        >
          Here, you can view all the details of the Property Requirement that you Posted
        </p>
      </div>
          <div
            style={{
              width: screenwidth <= 1200 ? screenwidth - 20 + "px" : null,
            }}
            className="compare"
          >
            <table>

              <tr>
                <td className="heading">CATEGORY</td>
                <td className="heading">POSTING INFO</td>
              </tr>
              <p className="title" >User Details</p>
              <tr>
                <td className="heading">User Email ID</td>
                <td>{userDetails.email}</td>
              </tr>
              <tr>
                <td className="heading">User Phone Number</td>
                <td>{userDetails.phonenumber}</td>
              </tr>
              <tr>
                <td className="heading">Property Owner Profile Number</td>
                <td>{userDetails._id}</td>
              </tr>
              <p className="title" >Personal Details</p>
              <tr>
                <td className="heading">Property Owner Name</td>
                <td>{userDetails.name}</td>
              </tr>

              <tr>
                <td className="heading">Alternative Phone Number</td>
                <td>{userDetails.Personal_Details.Alternative_Phone_Number}</td>
              </tr>
              <tr>
                <td className="heading">Alternative Email ID</td>
                <td>{userDetails.Personal_Details.Alternative_Email_Id}</td>
              </tr>
              <tr>
                <td className="heading">WhatsApp Number</td>
                <td>{userDetails.Personal_Details.Personal_WhatsApp_Number}</td>
              </tr>
              <p className="title" >Address</p>
              <tr>
                <td className="heading">
                  House Name / Building Name / Plot Name / Land Name
                </td>
                <td>{ProductDetails.Land_Building}</td>
              </tr>
              <tr>
                <td className="heading">State</td>
                <td>{ProductDetails.State}</td>
              </tr>
              <tr>
                <td className="heading">City</td>
                <td>{ProductDetails.City}</td>
              </tr>
              <tr>
                <td className="heading">Area Name</td>
                <td>{ProductDetails.Area_Name}</td>
              </tr>
              <tr>
                <td className="heading">Pin Code</td>
                <td>{ProductDetails.Pin_Code}</td>
              </tr>
              <p className="title" >Service Type Required</p>
              <tr>
                <td className="heading">Service Type</td>
                <td>{ProductDetails.Project_Type}</td>
              </tr>

              <tr>
                <td className="heading">Property Type</td>
                <td>
                  {ProductDetails.BuildingType} - {ProductDetails.Area_Type}
                </td>
              </tr>
              <tr>
                <td className="heading">Property Status</td>
                <td>{ProductDetails.Status_Of_Project}</td>
              </tr>
              <tr>
                <td className="heading">Service Requirement</td>
                <td>{ProductDetails.Service_Type_Required}</td>
              </tr>
              <p className="title" >Time Frame</p>
              <tr>
                <td className="heading">
                  How soon would you like to get the work started?
                </td>
                <td>{ProductDetails.Beginning_The_Project}</td>
              </tr>
              <p className="title" >Property Information</p>
              <tr>
                <td className="heading">Property Requirement Number</td>
                <td>{ProductDetails._id}</td>
              </tr>
              <tr>
                <td className="heading">Property Dimension Type</td>
                <td>{ProductDetails.Dimension_Type}</td>
              </tr>
              <tr>
                <td className="heading">Property Area</td>
                <td>{ProductDetails.Project_Area}sq. ft.</td>
              </tr>
              <tr>
                <td className="heading">Property Condition</td>
                <td>{ProductDetails.Property_Condition}</td>
              </tr>
              <p className="title" >Budget</p>
              <tr>
                <td className="heading">
                  What is your approximate budget for the scope of work
                  involved?
                </td>
                <td>{ProductDetails.Budget}</td>
              </tr>
              <p className="title" >Property Requirements</p>
              <tr>
                <td className="heading">
                  Please describe your property requirements completely and as
                  clearly as possible
                </td>
                <td>{ProductDetails.description}</td>
              </tr>
            </table>
          </div>
        </div>
        <br />
        <br />
        <br />
      </div>
    );
  } else {
    return <LoadingIcon />;
  }
}
