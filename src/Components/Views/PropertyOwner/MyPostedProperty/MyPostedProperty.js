import React, { useEffect, useLayoutEffect, useState } from "react";
import "./MyPostedProperty.css";
import { table, transactiondata } from "./Data";
import file from "./file.docx";
// import { useSelector } from "react-redux";
import LoadingIcon from "../../../utils/LoadingIcon/LoadingIcon";
import DownloadLead from "./DownloadLead";
import axios from "axios";

function MyPurchasedLeadTable() {

  // const user = useSelector(state => state.user)

  const bodywidth = document.querySelector("body").scrollWidth;

  const [screenwidth, setscreenwidth] = useState(bodywidth);

  useLayoutEffect(() => {
    window.addEventListener("resize", () => {
      setscreenwidth(window.innerWidth);
    });
  }, [screenwidth]);
  
  useEffect(() => {
    axios.get(`/api/users/mypostedproperty`)
      .then(response => {
        // console.log("Response ", response);
      })
  }, [])

  // if(user.userData && user.userData.isAuth)
  // {
  //   console.log(user.userData.history);
    return (
      <div className="mypostedproperty-transaction-table-container">
        <div style={{
          width:screenwidth <= 830 ? screenwidth-20 +'px' : null
        }} className="transaction-table">
          <table>
            <tr className="table-titles">
              {table.titles.map((title, index) => {
                return (
                  <th key={index} colSpan={title.colspan} rowSpan={title.rowspan}>
                    {title.Name}
                  </th>
                );
              })}
            </tr>
            <tr className="table-sub-titles">
              {table.subtitles.map((subtitle, index) => {
                return <td key={index}>{subtitle}</td>;
              })}
            </tr>
  
            {/* {user.userData.history.map((data, index) => {
              return (
                <tr className="transaction-data">
                  <td>{index + 1}</td>
                  <td>{data.writer}</td>
                  <td>{data.name}</td>
                  <td>{data.id}</td>
                  <td>{data.Land_Building}</td>
                  <td>{data.State}</td>
                  <td>{data.City}</td>
                  <td>{data.Project_type} - {data.Interior_type}{data.Construction_type}</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>0</td>
                  <td>0</td>
                  <td>0</td>
                  <td>0</td>
                  <td>0</td>
                  <td>0</td>
                  <td>0</td>
                  <td>0</td>
                  <td>0</td>
                  <td>0</td>
                  <td>0</td>
                  <td>
                      <DownloadLead />
                  </td>
                </tr>
              );
            })} */}
          </table>
        </div>
      </div>
    );
  // } else {
  //   return (
  //     <LoadingIcon />
  //   )
  // }
}
export default MyPurchasedLeadTable;
