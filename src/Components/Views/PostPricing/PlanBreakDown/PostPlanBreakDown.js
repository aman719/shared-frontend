import React from "react";
import Razorpay from "../../../utils/Razorpay/SP_subscription/Razorpay";
import "./PlanBreakDown.css";
import { BABY_BEAR_PRICE, MAMA_BEAR_PRICE, PAPA_BEAR_PRICE, TEDDY_BEAR_PRICE } from "../../../../PricingValidityConfig";

const PlanBreakDown = (props) => {
  const plan = props.plan;

  return (
    <div style={props.styles} className="plan-breakdown-container">
      <div className="plan-breakdown">
        <span onClick={props.handleclose} id="close" className="material-icons">
          cancel
        </span>
        <div className="plan-breakdown-details">
          <div className="plan-title-and-price">
            <p className="title">{plan.Name}</p>
            <p className="pricing">{plan.Price}</p>
          </div>
          <p className="features-title">Features :</p>
          <div className="features-container">
            <div className="features">
              {plan.Features.map((feature, index) => {
                return (
                  <p key={index} className="feature">
                    <span id="dot" class="material-icons">
                      fiber_manual_record
                    </span>
                    {feature}
                  </p>
                );
              })}
            </div>
            <div className="billing-details">
              <div className="fee-and-taxes">
                <h3 style={{ textAlign: 'center' }}>Amount Per Month</h3>
                <br />
                <div className="fee">
                  <p className="title">Regular Leads:</p>
                  <p className="amount">{plan.Name === "Baby Bear" ? 29 : plan.Name === "Baby Tiger" ? 13 : plan.Name === "Startup Bear" ? 4 : plan.Name === "Startup Tiger" ? 2 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title" style={{color: 'green'}}>Extra FREE Leads:</p>
                  <p className="amount" style={{color: 'green'}}>{plan.Name === "Baby Bear" ? 6 : plan.Name === "Baby Tiger" ? 4 : plan.Name === "Startup Bear" ? 2 : plan.Name === "Startup Tiger" ? 1 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">Total No. of Leads:</p>
                  <p className="amount">{plan.Name === "Baby Bear" ? 35 : plan.Name === "Baby Tiger" ? 17 : plan.Name === "Startup Bear" ? 7 : plan.Name === "Startup Tiger" ? 3 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title" style={{color: 'red'}}>Original Cost Per Regular Lead:</p>
                  <p className="amount" style={{color: 'red'}}><del>₹ {plan.Name === "Baby Bear" ? 700 : plan.Name === "Baby Tiger" ? 1400 : plan.Name === "Startup Bear" ? 7 : plan.Name === "Startup Tiger" ? 3 : 0}</del></p>
                </div>
                <div className="fee">
                  <p className="title" style={{color: '#1F80E0'}}>Discounted Cost Per Regular Lead:</p>
                  <p className="amount" style={{color: '#1F80E0'}}>₹ {plan.Name === "Baby Bear" ? 250 : plan.Name === "Baby Tiger" ? 450 : plan.Name === "Startup Bear" ? 7 : plan.Name === "Startup Tiger" ? 3 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">Platform Service Ops & Adjustment Fee:</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? -751 : plan.Name === "Baby Tiger" ? 649 : plan.Name === "Startup Bear" ? 99 : plan.Name === "Startup Tiger" ? 49 : 0}</p>
                </div>
                <br />
                <h3 style={{ textAlign: 'center' }}>Duration</h3>
                <br />
                <div className="fee">
                  <p className="title">No. of Days:</p>
                  <p className="amount">{plan.Name === "Baby Bear" ? 30 : plan.Name === "Papa Bear" ? 3 : plan.Name === "Mama Bear" ? 6 : plan.Name === "Teddy Bear" ? 12 : plan.Name === "Baby Tiger" ? 30 : plan.Name === "Startup Bear" ? 7 : plan.Name === "Startup Tiger" ? 7 : 0}</p>
                </div>
                <br />
                <h3 style={{ textAlign: 'center' }}>Amount To Be Paid</h3>
                <br />
                <div className="fee">
                  <p className="title" style={{color: 'red'}}>Original Amount (Exclusive of G.S.T.)</p>
                  <p className="amount" style={{color: 'red'}}><del>₹ {plan.Name === "Baby Bear" ? 12998 : plan.Name === "Papa Bear" ? PAPA_BEAR_PRICE : plan.Name === "Mama Bear" ? MAMA_BEAR_PRICE : plan.Name === "Teddy Bear" ? TEDDY_BEAR_PRICE : plan.Name === "Baby Tiger" ? 25996 : plan.Name === "Startup Bear" ? 799 : plan.Name === "Startup Tiger" ? 799 : 0}</del></p>
                </div>
                <div className="fee">
                  <p className="title">Discounted Amount (Exclusive of G.S.T.)</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? 6499 : plan.Name === "Papa Bear" ? PAPA_BEAR_PRICE : plan.Name === "Mama Bear" ? MAMA_BEAR_PRICE : plan.Name === "Teddy Bear" ? TEDDY_BEAR_PRICE : plan.Name === "Baby Tiger" ? 6499 : plan.Name === "Startup Bear" ? 799 : plan.Name === "Startup Tiger" ? 799 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title">G.S.T. Amount (18%)</p>
                  <p className="amount">₹ {plan.Name === "Baby Bear" ? 0 : plan.Name === "Papa Bear" ? PAPA_BEAR_PRICE : plan.Name === "Mama Bear" ? MAMA_BEAR_PRICE : plan.Name === "Teddy Bear" ? TEDDY_BEAR_PRICE : plan.Name === "Baby Tiger" ? 0 : plan.Name === "Startup Bear" ? 799 : plan.Name === "Startup Tiger" ? 799 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title" style={{color: '#1F80E0'}}>Discounted Amount (Inclusive of G.S.T.)</p>
                  <p className="amount" style={{color: '#1F80E0'}}>₹ {plan.Name === "Baby Bear" ? 6499 : plan.Name === "Papa Bear" ? PAPA_BEAR_PRICE : plan.Name === "Mama Bear" ? MAMA_BEAR_PRICE : plan.Name === "Teddy Bear" ? TEDDY_BEAR_PRICE : plan.Name === "Baby Tiger" ? 6499 : plan.Name === "Startup Bear" ? 799 : plan.Name === "Startup Tiger" ? 799 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title" style={{color: 'green'}}>Non-Prime Member Savings</p>
                  <p className="amount" style={{color: 'green'}}>₹ {plan.Name === "Baby Bear" ? 6499 : plan.Name === "Papa Bear" ? PAPA_BEAR_PRICE : plan.Name === "Mama Bear" ? MAMA_BEAR_PRICE : plan.Name === "Teddy Bear" ? TEDDY_BEAR_PRICE : plan.Name === "Baby Tiger" ? 19497 : plan.Name === "Startup Bear" ? 799 : plan.Name === "Startup Tiger" ? 799 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title" style={{color: 'green'}}>NebourHood Prime S.P. Savings</p>
                  <p className="amount" style={{color: 'green'}}>₹ {plan.Name === "Baby Bear" ? 0 : plan.Name === "Papa Bear" ? PAPA_BEAR_PRICE : plan.Name === "Mama Bear" ? MAMA_BEAR_PRICE : plan.Name === "Teddy Bear" ? TEDDY_BEAR_PRICE : plan.Name === "Baby Tiger" ? 0 : plan.Name === "Startup Bear" ? 799 : plan.Name === "Startup Tiger" ? 799 : 0}</p>
                </div>
                <div className="fee">
                  <p className="title" style={{color: 'green'}}>Total Savings</p>
                  <p className="amount" style={{color: 'green'}}>₹ {plan.Name === "Baby Bear" ? 6499 : plan.Name === "Papa Bear" ? PAPA_BEAR_PRICE : plan.Name === "Mama Bear" ? MAMA_BEAR_PRICE : plan.Name === "Teddy Bear" ? TEDDY_BEAR_PRICE : plan.Name === "Baby Tiger" ? 19497 : plan.Name === "Startup Bear" ? 799 : plan.Name === "Startup Tiger" ? 799 : 0}</p>
                </div>
                <br />
                <div>
                  <p style={{ fontSize: '1em' }}><span style={{ color: 'green' }}>You can save an additional Rs. 1,500 on this order</span> & unlock exclusive benefits and rewards by becoming a NebourHood Prime S.P. Member. Click here to Join Now!</p>
                </div>
              </div>
              <br />
              <button className="proceed">
                <Razorpay
                  Name={plan.Name}
                  Price={plan.Price}
                  Validity={plan.Validity}
                />
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PlanBreakDown;
