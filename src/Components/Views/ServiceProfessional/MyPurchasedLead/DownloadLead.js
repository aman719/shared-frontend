import React from "react";
import { useDispatch } from "react-redux";
import { DownloadLeadPDF } from "../../../../_actions/company_actions";
import jsPDF from 'jspdf';
import imgData from '../../../asserts/Property_Requirement_Images/Apartment.jpg'


function DownloadLead(props) {
  const dispatch = useDispatch();

  const DownloadLeadDetails = (ProductId, UserID) => {
    // console.log("Downlaod Leads", ProductId);
    let variables = {
      LeadId: ProductId,
      UserID: UserID,
    };
    dispatch(DownloadLeadPDF(variables)).then((response) => {
      functioncall(response.payload.result.product);
      // console.log("Response", response);
    });
  };

  function functioncall(data) {
    // console.log("Function response", data);
    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "landscape"; // portrait or landscape
    
    const marginLeft = 40;

    const pdf = new jsPDF(orientation, unit, size);

    const headers = [[
                      "Property Owner Name", 
                      // "Email ID",
                      // "Phone Number",
                      // "Alternative Email ID",
                      // "Alternative Phone Number",
                      // "Personal WhatsApp Number",
                      // "Property Owner Profile Number",
                      // "House Name / Building Name / Plot Name / Land Name",
                      // "State",
                      // "City",
                      // "Area Name",
                      // "Pin Code",
                      // "Service Type",
                      // "Property Type",
                      // "Property Status",
                      // "Service Requirement",
                      // "How soon would you like to get the work started?",
                      // "Property Requirement Number",
                      // "Property Dimension Type",
                      // "Property Area",
                      // "Property Condition",
                      // "What is your approximate budget for the scope of work involved?",
                      // "Please describe your property requirements completely and as clearly as possible:",
                    ]];
    pdf.text("Lead Details", marginLeft, 40);

    let content = {
      startY: 50,
      head: headers,
      body: [
        [ 
          data.Name_Of_property_Owner,
          data.User_Email,
          data.Alternative_Email,
          data.Alternative_Number,
          data.Area_Name,
          data.Budget,
          data.BuildingType,
          data.City,
          data.Dimension_Type,
          data.Land_Building,
          data.Phone_Number,
          data.Pin_Code,
          data.Project_Area,
          data.Project_Type,
          data.Property_Condition,
          data.Service_Type_Required,
          data.State,
          data.Status_Of_Project,
          data.WhatsApp,
          data.description,
          data._id,
        ]
      ]
    };

    
    // pdf.addImage(imgData, 'JPG', 0, 0);
    pdf.autoTable(content);
    pdf.save(data._id,);
  }


  return (
    <span
      className="material-icons"
      onClick={() => DownloadLeadDetails(props.ProductId, props.UserID)}
    >
      download
    </span>
  );
}

export default DownloadLead;
