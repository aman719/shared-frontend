import React, { useEffect } from "react";
import { table } from "./Data";
import "./MembershipInvoice.css";
import logo from "../../NavBar/DesktopNav/Logo.png";
import PleaseNoteDisclaimer from "../../../utils/PleaseNoteDisclaimer/PleaseNoteDisclaimer";

const MembershipInvoice = ({ InvoiceDetails }) => {

  useEffect(() => {
    // console.log(InvoiceDetails)
  }, [InvoiceDetails])

  return (
    <div id="plans-invoice-container" className="invoice-container">
      <img src={logo} alt="logo" />
      <p className="upper">Thank you for placing your order with us!</p>
      <p className="upper">
        We received your order and have completed it successfully. Your
        NebourHood Invoice Payment ID is <b>{InvoiceDetails.InvoicePaymentId}</b>
      </p>
      <br />
      <p className="upper">
        The details of the NebourHood Prime Service Professional (S.P.) Membership Subscription Plan that you purchased from NebourHood are as follows:{" "}
      </p>
      <br />
      <table id="tablee">
        <tr className="table-titles">
          {table.titles.map((title, index) => {
            return (
              <th key={index} colSpan={title.colspan} rowSpan={title.rowspan}>
                {title.Name}
              </th>
            );
          })}
        </tr>
        <tr className="table-sub-titles">
          {table.subtitles.map((subtitle, index) => {
            return <td key={index}>{subtitle}</td>;
          })}
        </tr>

        <tr className="transaction-data">
          <td>{1}</td>
          <td>{InvoiceDetails.InvoicePaymentId}</td>
          <td>{InvoiceDetails.PurchaseOrderId}</td>
          <td>{InvoiceDetails.DateandTimeOfPayment}</td>
          <td>{InvoiceDetails.SubscribedPlanDetails.PlanName}</td>
          <td>{InvoiceDetails.SubscribedPlanDetails.PlanValidity}</td>
          <td>{InvoiceDetails.SubscribedPlanDetails.OriginalExclusiveOfGST}</td>
          <td>{InvoiceDetails.SubscribedPlanDetails.DiscountedExclusiveOfGST}</td>
          <td>{InvoiceDetails.SubscribedPlanDetails.GstAmount}</td>
          <td>{InvoiceDetails.SubscribedPlanDetails.InsclusiveOfGST}</td>
          <td>{InvoiceDetails.SubscribedPlanDetails.TotalSavings}</td>
          {/* <td>{InvoiceDetails.SubscribedPlanDetails.PlanExpiresOn}</td> */}
          <td>{InvoiceDetails.SubscribedPlanDetails.PlanStatus}</td>
          <td>{InvoiceDetails.PaymentStatus}</td>
          <td>{InvoiceDetails.PaymentMode}</td>
        </tr>
      </table>
      <br />
      <div style={{ marginLeft: '30px' }}>
        <div>
          <p>Thank you for being a valued NebourHood Prime S.P. Member! If you haven’t already, you can click here to Renew your Prime Membership Now!</p>
          <br />
        </div>
        <div>
            <p style={{ fontWeight: 'bold' }}>
              Plan Status:
            </p>
          </div>
          <div>
          <p>•	Active: The subscribed plan is active. </p>
          <p>•	Expired: The subscribed plan has expired. </p>
          <p>•	Pending Activation: The next subscribed plan would automatically activate after the current active plan expires.</p>
          </div>
      </div>
          <br />
      <br />
          <PleaseNoteDisclaimer />
      <br />
      <br />
      <p>For more details about your purchase, please visit <span style={{ color: '#1DA1F2' }} >nebourhood.com</span> and check for your order under the <span style={{ color: '#1DA1F2' }}>Transaction Details</span> section of your profile.</p>
      <br />
      <p>We’re glad that you found what you were looking for. It is our goal that you are always happy with what you bought from us. We look forward to seeing you again.</p>
      <br />
      <p>Thank you,</p>
      <p>Team NebourHood.</p>
      <br />
    </div>
  );
};

export default MembershipInvoice;
