import React from "react";

function DownloadPlan(props) {
  const OnDownloadPlanPDF = (planId) => {
    // console.log("PDF Download ", planId);
  };

  return (
    <span
      className="material-icons"
      onClick={() => OnDownloadPlanPDF(props.PlanId)}
    >
      download
    </span>
  );
}

export default DownloadPlan;
