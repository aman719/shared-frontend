import React from 'react'
import PageBanner from '../../utils/PageBanner/PageBanner'

function ReplacementRefundCancellation() {
  return (
    <div>
    <PageBanner
      title="Replacement, Refund & Cancellation Policies"
    />
  </div>
  )
}

export default ReplacementRefundCancellation